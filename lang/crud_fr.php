<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/crud/lang
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'erreur_action_erronee' => 'CRUD : action @action@ erronée',
	'erreur_action_inconnue_table' => 'CRUD : action @action@ inconnue pour table @table@',
	'erreur_article_inconnue' => 'CRUD : l’article @id@ n’existe pas',
	'erreur_creation' => 'CRUD : impossible de créer l’objet de type "@objet@" (Vérifiez vos droits)',
	'erreur_info_obligatoire' => 'CRUD : Le champ @info@ est obligatoire',
	'erreur_objet_inexistant' => 'CRUD : l’objet @objet@ #@id_objet@ n’existe pas',
	'erreur_rubrique_inconnue' => 'CRUD : la rubrique @id@ n’existe pas',
	'erreur_suppression' => 'CRUD : erreur de suppression de l’objet "@objet@" #@id_objet@ (Vérifiez vos droits)',
	'erreur_table_erronee' => 'CRUD : table @table@ erronée',
	'erreur_table_inconnue' => 'CRUD : table @table@ inconnue',
	'erreur_update' => 'CRUD : erreur de mise à jour de l’objet "@objet@" #@id@ (Vérifiez vos droits)'
);
