<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/crud?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'erreur_action_erronee' => 'CRUD: wrong action @action@',
	'erreur_action_inconnue_table' => 'CRUD: unknown action @action@ for the table @table@',
	'erreur_article_inconnue' => 'CRUD: the article #@id@ doesn’t exist',
	'erreur_creation' => 'CRUD: impossible to create the object of type "@objet@" (Check your rights)',
	'erreur_info_obligatoire' => 'CRUD: the field @info@ is mandatory',
	'erreur_objet_inexistant' => 'CRUD: the object @objet@ #@id_objet@ doesn’t exist',
	'erreur_rubrique_inconnue' => 'CRUD: the section @id@ doesn’t exist',
	'erreur_suppression' => 'CRUD: error deleting the object "@objet@" #@id_objet@ (Check your rights)',
	'erreur_table_erronee' => 'CRUD: wrong table @table@',
	'erreur_table_inconnue' => 'CRUD: unknown table @table@',
	'erreur_update' => 'CRUD: error updating the object "@objet@" #@id@ (Check your rights)'
);
