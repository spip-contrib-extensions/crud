<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-crud?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'crud_description' => 'Een C(r)UD interface voor SPIP die kan worden gebruikt door middel van een in de URL gedefinieerde actie of een directe aanroep.',
	'crud_slogan' => 'Interface voor het maken, aanpassen en verwijderen van een object'
);
