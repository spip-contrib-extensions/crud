<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-crud?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'crud_description' => 'Uma interface C(r)UD para o SPIP que pode ser usada como intermediária de uma ação definida pelo seu URL, ou por chamada direta.',
	'crud_slogan' => 'Interface de criação, atualização e exclusão de um objeto'
);
