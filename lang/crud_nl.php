<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/crud?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'erreur_action_erronee' => 'CRUD: foutieve actie @action@',
	'erreur_action_inconnue_table' => 'CRUD: onbekende actie @action@ voor tabel @table@',
	'erreur_article_inconnue' => 'CRUD: artikel @id@ bestaat niet',
	'erreur_creation' => 'CRUD:  het object met type "@objet@" kan niet worden gemaakt (Controleer je rechten)',
	'erreur_info_obligatoire' => 'CRUD: Het veld @info@ is verplicht',
	'erreur_objet_inexistant' => 'CRUD: object @objet@ #@id_objet@ bestaat niet',
	'erreur_rubrique_inconnue' => 'CRUD: rubriek @id@ bestaat niet',
	'erreur_suppression' => 'CRUD: fout bij verwijdering van object "@objet@" #@id_objet@ (Controleer je rechten)',
	'erreur_table_erronee' => 'CRUD: foutieve tabel @table@',
	'erreur_table_inconnue' => 'CRUD: onbekende tabel @table@',
	'erreur_update' => 'CRUD: fout bij het aanpassen van object "@objet@" #@id@ (Controleer je rechten)'
);
