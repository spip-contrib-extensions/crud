<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-crud?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'crud_description' => 'Una interfaz C(r)UD para SPIP que se puede utilizar mediante una acción definida por su url o una llamada directa',
	'crud_slogan' => 'Interfaz de creación, actualización y eliminación de un objeto'
);
