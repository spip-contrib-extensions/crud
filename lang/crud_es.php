<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/crud?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'erreur_action_erronee' => 'CRUD: acción @action@ errónea',
	'erreur_action_inconnue_table' => 'CRUD: acción @action@ desconocida para tabla @table@',
	'erreur_article_inconnue' => 'CRUD: el artículo @id@ no existe',
	'erreur_creation' => 'CRUD: imposible crear el objeto de tipo "@objet@" (Verifique sus derechos)',
	'erreur_info_obligatoire' => 'CRUD: El campo @info@ es obligatorio',
	'erreur_objet_inexistant' => 'CRUD: el objeto @objet@ #@id_objet@ no existe',
	'erreur_rubrique_inconnue' => 'CRUD: la sección @id@ no existe',
	'erreur_suppression' => 'CRUD: error de eliminación del objeto "@objet@" #@id_objet@ (Verifique sus derechos)',
	'erreur_table_erronee' => 'CRUD: tabla @table@ errónea',
	'erreur_table_inconnue' => 'CRUD: tabla @table@ desconocida',
	'erreur_update' => 'CRUD: error de actualización del objeto "@objet@" #@id@ (Verifique sus derechos)'
);
