<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/crud?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'erreur_action_erronee' => 'CRUD: ação @action@ incorreta',
	'erreur_action_inconnue_table' => 'CRUD: ação @action@ desconhecida para a tabela @table@',
	'erreur_article_inconnue' => 'CRUD: a matéria @id@ não existe',
	'erreur_creation' => 'CRUD: não foi possível criar o objeto do tipo "@objet@" (verifique as suas credenciais)',
	'erreur_info_obligatoire' => 'CRUD: o campo @info@ é obrigatório',
	'erreur_objet_inexistant' => 'CRUD: o objeto @objet@ #@id_objet@ não existe',
	'erreur_rubrique_inconnue' => 'CRUD: a seção @id@ não existe',
	'erreur_suppression' => 'CRUD: erro de exclusão do objeto "@objet@" #@id_objet@ (verifique as suas credenciais)',
	'erreur_table_erronee' => 'CRUD: tabela @table@ incorreta',
	'erreur_table_inconnue' => 'CRUD: tabela @table@ desconhecida',
	'erreur_update' => 'CRUD: erro de atualização do objeto "@objet@" #@id@ (verifique as suas credenciais)'
);
